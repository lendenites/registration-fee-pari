package steps;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

import webCapability.DateAndTimeUtility;

public class RegistrationFees 
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]> data;
	DateAndTimeUtility setTime=new DateAndTimeUtility();
	String MonitoringData[]= {"RegistrationFees","",""};
	String MonitoringCol[]= {"TotalCount","",""};

	String MonitoringRow[]= {"",""};
	//String rowSpace = {null};
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6NDEsImZpbHRlciI6WyJhbmQiLFsiYmV0d2VlbiIsWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMjExN10sIm1pbnV0ZSJdLCIyMDIyLTA0LTI3VDA4OjAwOjAwIiwiMjAyMi0wNC0yN1QxMDowMDowMCJdLFsiPSIsWyJmaWVsZC1pZCIsMjExNl0sIkNyZWRpdCJdLFsiPSIsWyJmaWVsZC1pZCIsMjExOF0sNDUwLDUwMF1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJkYXRhYmFzZSI6Mn0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7InRhYmxlLmNvbHVtbl93aWR0aHMiOltudWxsLG51bGwsMjgzXX19";
	//above Url for every 2hours

	String url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjQxLCJmaWx0ZXIiOlsiYW5kIixbIj0iLFsiZmllbGQtaWQiLDIxMThdLDUwMCw0NTBdLFsiPSIsWyJmaWVsZC1pZCIsMjExNl0sIkNyZWRpdCJdLFsidGltZS1pbnRlcnZhbCIsWyJmaWVsZC1pZCIsMjExN10sImN1cnJlbnQiLCJkYXkiXV0sImFnZ3JlZ2F0aW9uIjpbWyJjb3VudCJdXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6eyJ0YWJsZS5jb2x1bW5fd2lkdGhzIjpbbnVsbCxudWxsLDI4M119fQ==";
	//for whole day
	public RegistrationFees(WebDriver driver, CSVWriter writer, List<String[]> data) throws Exception
	{
		this.driver=driver;
		this.data=data;
		driver.get(Url);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		setTime.setCurrentDateWithTime(driver);
		MonitoringData[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		Thread.sleep(5000);
		writer.writeNext(MonitoringData);
		writer.flush();
		Thread.sleep(2000);
		
		writer.writeNext(MonitoringRow);
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get(url);
		Thread.sleep(5000);
		MonitoringCol[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		writer.writeNext(MonitoringCol);

		Thread.sleep(2000);
		data.add(new String[] {"","All data on "+setTime.getCurrentDate()+" till at "+setTime.getCurrentTime()});
		data.add(new String[] {"MonitorName","Count",""});
		writer.flush();
	}

}
