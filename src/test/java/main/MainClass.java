package main;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.opencsv.CSVWriter;

import steps.Login;
import steps.RegistrationFees;
import webCapability.Capability;
import webCapability.ExcelFile;

public class MainClass extends Capability
{
	public List<String[]> data=new ArrayList<String[]>();
	CSVWriter writer;
	ExcelFile excelFile=new ExcelFile();
	WebDriver driver;
	@BeforeTest
	public void openchrome() throws Exception
	{
		this.driver = WebCapability();
		this.writer=excelFile.ExcelSheet();
		driver.manage().window().maximize();

	}
	@Test(priority = 1)
	public void login() throws InterruptedException
	{
		new Login(driver);
	}
	@Test(priority = 2)
	public void RF() throws Exception
	{
	new RegistrationFees(driver, writer, data);	
	}
	@AfterTest
	public void CloseBrowser()
	{
		writer.writeAll(data);
		driver.quit	();
	}
}
